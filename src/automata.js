require('./util.js');
import { DataSet } from 'vis/index-network';


export default class Automata {

    static get NODE_TYPES() {
        return {
            START: 0,
            FINAL: 1,
            STATE: 3,
            TRANSITION: 5,
            LOOP: 7,
            FINAL_CURRENT: 2,
            STATE_CURRENT: 6,
            TRANSITION_CURRENT: 10,
            LOOP_CURRENT: 14
        }
    }

    constructor(transitions, start, final) {
        this._start = start;
        this._final = new Set();
        final.split(',').forEach(s => {
            this._final.add(s.trim());
        });
        this._table = {};
        this._states = {};
        this._lambdaTable = {};
        this._hasLambdas = false;

        transitions.trim().split('\n').forEach(transition => {
            let triplet = transition.split(' ');
            if (triplet.length !== 3)
                throw new Error('Transición inválida: ' + transition);
            let from = triplet[0],
                to = triplet[2],
                char = triplet[1];
            if (char.length !== 1)
                throw new Error('Símbolo inválido: ' + char);
            if (char === '_') {
                if (from === to)
                    console.error('Eliminada la transición ' + transition);
                else {
                    if (!(from in this._lambdaTable)) this._lambdaTable[from] = {};
                    this._lambdaTable[from][to] = true;
                    this._hasLambdas = true;
                }
            } else {
                if (!(from in this._table)) this._table[from] = {};
                if (!(char in this._table[from])) this._table[from][char] = {};
                this._table[from][char][to] = true;
            }
            this._states[from] = false;
            this._states[to] = false;
        });

        if (!(start in this._table) && !(start in this._lambdaTable))
            throw new Error('Ninguna transición definida desde el estado inicial.');

        this._nStates = Object.keys(this._states).length;
    }

    static makeTransitionNodeId(from, symbol, to) {
        return '{from}--{to}'.format({
            from: from,
            symbol: symbol,
            to: to
        });
    }

    hasLambdaTransitions() { return this._hasLambdas; }

    doStep(states, symbol) {
        let transitions = new Set();
        let newStates = new Set();

        for (const state of states) {
            if (state in this._table) {
                if (symbol in this._table[state]) {
                    Object.keys(this._table[state][symbol]).forEach(newState => {
                        newStates.add(newState);
                        transitions.add(Automata.makeTransitionNodeId(state, symbol, newState));
                    })
                }
            }
        }

        return {
            states: [...newStates],
            transitions: [...transitions]
        };
    }

    doLambdaStep(states) {
        let currentStates = states;
        let transitions = new Set();

        do {
            let newStates = new Set([...currentStates]);

            for (const state of currentStates) {
                if (state in this._lambdaTable) {
                    Object.keys(this._lambdaTable[state]).forEach(newState => {
                        newStates.add(newState);
                        transitions.add(Automata.makeTransitionNodeId(state, '', newState));
                    })
                }
            }

            if (newStates.size === currentStates.size) break;
            currentStates = new Set([...newStates]);
        } while (true);

        return {
            states: [...currentStates],
            transitions: [...transitions]
        }

    }

    compute(string) {

        const input = Array.from(string);

        let currentStates = new Set([this._start]);
        let computation = [];

        if (this.hasLambdaTransitions()) {

            let lambdaStep = this.doLambdaStep(currentStates);

            computation.push({
                transitions: ["__start__"],
                states: [this._start],
                lambdaTransitions: [...lambdaStep.transitions],
                lambdaStates: [...lambdaStep.states]
            });

            currentStates = new Set([...lambdaStep.states])

        } else {
            computation.push({
                transitions: ["__start__"],
                states: [this._start]
            });
        }


        for (let symbol of input) {

            if (this.hasLambdaTransitions()) {

                let step = this.doStep(currentStates, symbol);
                let lambdaStep = this.doLambdaStep(step.states);

                computation.push({
                    transitions: [...step.transitions],
                    states: [...step.states],
                    lambdaTransitions: [...step.states, ...lambdaStep.transitions],
                    lambdaStates: [...lambdaStep.states]
                });

                currentStates = new Set([...lambdaStep.states]);

            } else {

                let step = this.doStep(currentStates, symbol);

                computation.push({
                    transitions: [...step.transitions],
                    states: [...step.states]
                });

                currentStates = new Set([...step.states]);

            }

        }

        return {
            accept: new Set([...currentStates].filter(x => this._final.has(x))).size > 0,
            path: computation
        };
    }

    getDataSets() {
        return ({
            nodes: this.getNodesDataSet(),
            edges: this.getEdgesDataSet()
        })
    }

    getNodesDataSet() {
        let stateNodes = Object.keys(this._states)
            .map(k => {
                return {
                    id: k,
                    label: k,
                    group: this._final.has(k) ?
                        Automata.NODE_TYPES.FINAL :
                        Automata.NODE_TYPES.STATE
                }
            })
            .concat([{id: '__start__', group: Automata.NODE_TYPES.START}]);

        const transitionNodes = {};
        const loopNodes = {};

        Object.keys(this._table).forEach(from => {
            Object.keys(this._table[from]).forEach(symbol => {
                Object.keys(this._table[from][symbol]).forEach(to => {
                    let id = Automata.makeTransitionNodeId(from, symbol, to);
                    let loop = to === from;
                    if (!(id in (loop?loopNodes:transitionNodes)))
                        (loop?loopNodes:transitionNodes)[id] = [];
                    (loop?loopNodes:transitionNodes)[id].push(symbol);
                })
            })
        });
        Object.keys(this._lambdaTable).forEach(from => {
            Object.keys(this._lambdaTable[from]).forEach(to => {
                let id = Automata.makeTransitionNodeId(from, '', to);
                if (!(id in transitionNodes))
                    transitionNodes[id] = [];
                transitionNodes[id].push('ε');
            })
        });

        return new DataSet([
            ...stateNodes,
            ...(Object.entries(transitionNodes).map(([nodeId, symbols]) => ({
                id: nodeId,
                label: symbols.join(','),
                group: Automata.NODE_TYPES.TRANSITION
            }))),
            ...(Object.entries(loopNodes).map(([nodeId, symbols]) => ({
                id: nodeId,
                label: symbols.join(','),
                group: Automata.NODE_TYPES.LOOP
            })))
        ]);
    }

    getEdgesDataSet() {

        const width = (e) => {
            e.width = 7; return e;
        };

        const curved = (e) => {
            e.smooth = { enabled: true, roundness: .5, type: 'curvedCCW' }; return e;
        };

        const makeEdge = (from, symbol, to) => [
            {
                from: from,
                to: Automata.makeTransitionNodeId(from, symbol, to),
                color: { inherit: 'to' }
            },
            {
                from: Automata.makeTransitionNodeId(from, symbol, to),
                to: to,
                color: { inherit: 'from' },
                arrows: { to: true }
            }
        ];

        const makeLambdaEdge = (from, to) => [
            {
                from: from,
                to: Automata.makeTransitionNodeId(from, '', to),
                color: { inherit: 'to' }

            },
            {
                from: Automata.makeTransitionNodeId(from, '', to),
                to: to,
                color: { inherit: 'from' },
                arrows: { to: true }
            }
        ];

        let arr = [{
            from: '__start__',
            to: this._start,
            arrows: {to: true},
            width: 15
        }];

        Object.keys(this._table).forEach(from => {
            Object.keys(this._table[from]).forEach(symbol => {
                Object.keys(this._table[from][symbol]).forEach(to => {
                    makeEdge(from, symbol, to).forEach(e => {
                        arr.push(from === to ? width(curved(e)) : width(e))
                    });
                });
            });
        });

        Object.keys(this._lambdaTable).forEach(from => {
            Object.keys(this._lambdaTable[from]).forEach(to => {
                makeLambdaEdge(from, to).forEach(e => arr.push(
                    from === to ? width(curved(e)) : width(e)
                ));
            });
        });

        return new DataSet(arr);
    }
}