String.prototype.format = String.prototype.format ||
    function () {
        let str = this.toString();
        if (arguments.length) {
            let t = typeof arguments[0];
            let key;
            let args = ("string" === t || "number" === t) ?
                Array.prototype.slice.call(arguments)
                : arguments[0];

            for (key in args) {
                str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
            }
        }

        return str;
    };



const arrayRange = (size, startAt = 0) => [...Array(size).keys()].map(i => i + startAt);

const wait = (time = 1000) => new Promise((resolve, reject) => {
    setTimeout(resolve, time)
});

// wait(1000)
//     .then(() => {
//         console.log('I promised to run after 1s');
//         return wait(1000);
//     })
//     .then(() => console.log('I promised to run after 2s'));