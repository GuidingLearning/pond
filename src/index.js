"use strict";

import './style.css';
window.$ = window.jQuery = require('jquery');

import { Network } from 'vis/index-network';
import Automata from './automata.js';

const COLORS = {
    RED: '#db3236',
    YELLOW: '#f4c20d',
    BLUE: '#4885ed',
    GREEN: '#3cba54',
    WHITE: '#ffffff',
    BLACK: '#000000'
};

let body = ($('body'));

let networkDiv = $('<div>')
    .addClass('network')
    .appendTo(body);

let panelDiv = $('<div>')
    .addClass('panel')
    .appendTo(body);

let automatonDiv = $('<textarea>')
    .attr('rows', 10)
    .attr('cols', 7)
    .appendTo(panelDiv);

$('<label>').attr('for', 'start-input').text('Initial State').appendTo(panelDiv);
let startInput = $('<input>').val('q0').attr('id', 'start-input').appendTo(panelDiv);

$('<label>').attr('for', 'final-input').text('Final State(s)').appendTo(panelDiv);
let finalInput = $('<input>').val('qf').attr('id', 'final-input').appendTo(panelDiv);

const stateImages = {};

const stateImage = (settings) => {
    let imageId = btoa(JSON.stringify(settings));
    if (!(imageId in stateImages)) {
        const r = 100;
        let svgComponents = [];
        svgComponents.push('<svg xmlns="http://www.w3.org/2000/svg" width="{width}" height="{height}">');
        svgComponents.push('<circle r="{r}" cx="{cx}" cy="{cy}" fill="{fill}" stroke-width="15" stroke="{stroke}">'
            .format({
                cx: r,
                cy: r,
                r: r,
                fill: settings.current ? (settings.final ? COLORS.GREEN : COLORS.RED) : COLORS.WHITE,
                stroke: settings.selected ? COLORS.BLUE : COLORS.BLACK
            }));
        svgComponents.push('</circle>');
        if (settings.final) {
            svgComponents.push('<circle r="{r}" cx="{cx}" cy="{cy}" fill="#ffffff00" stroke-width="7" stroke="{stroke}">'
                .format({
                    cx: r,
                    cy: r,
                    r: r*.75,
                    // fill: settings.current ? COLORS.GREEN : COLORS.WHITE,
                    stroke: settings.selected ? COLORS.BLUE : COLORS.BLACK
                }));
            svgComponents.push('</circle>');
        }
        svgComponents.push('</svg>');
        const DOMURL = window.URL || window.webkitURL || window;
        const svgString = svgComponents.join('').format({
            width: r*2,
            height: r*2
        });
        let svg = new Blob([svgString], {type: 'image/svg+xml;charset=utf-8'});
        stateImages[imageId] = DOMURL.createObjectURL(svg);
    }
    return stateImages[imageId];
};

function drawAutomaton(container, data) {

    const transitionMass = 3;
    const stateMass = 20;
    const loopMass = 15;
    const startMass = 2;

    let options = {
        nodes: {
            size: 100,
            font: {
                size: 50,
                face: 'monospace'
            }
        },
        edges: {
            arrowStrikethrough: false,
            smooth: false,
            color: {
                color: COLORS.BLACK,
                highlight: COLORS.BLUE,
                inherit: false
            }
        },
        interaction: {
            selectConnectedEdges: false
        },
        groups: {
            useDefaultGroups: false,
            [Automata.NODE_TYPES.START]: {
                shape: 'text',
                mass: startMass
            },
            [Automata.NODE_TYPES.FINAL]: {
                shape: 'circularImage',
                image: {
                    selected: stateImage({selected: true, final: true}),
                    unselected: stateImage({selected: false, final: true})
                },
                mass: stateMass
            },
            [Automata.NODE_TYPES.STATE]: {
                shape: 'circularImage',
                image: {
                    selected: stateImage({selected: true, final: false}),
                    unselected: stateImage({selected: false, final: false})
                },
                mass: stateMass
            },
            [Automata.NODE_TYPES.TRANSITION]: {
                shape: 'box',
                borderWidth: 3,
                shapeProperties: { borderRadius: 0 },
                color: { border: COLORS.BLACK, background: COLORS.WHITE },
                mass: transitionMass
            },
            [Automata.NODE_TYPES.LOOP]: {
                shape: 'box',
                borderWidth: 3,
                shapeProperties: { borderRadius: 0 },
                color: { border: COLORS.BLACK, background: COLORS.WHITE },
                mass: loopMass
            },
            [Automata.NODE_TYPES.FINAL_CURRENT]: {
                shape: 'circularImage',
                image: {
                    selected: stateImage({selected: true, final: true, current: true}),
                    unselected: stateImage({selected: false, final: true, current: true})
                },
                mass: stateMass
            },
            [Automata.NODE_TYPES.STATE_CURRENT]: {
                shape: 'circularImage',
                image: {
                    selected: stateImage({selected: true, final: false, current: true}),
                    unselected: stateImage({selected: false, final: false, current: true})
                },
                mass: stateMass
            },
            [Automata.NODE_TYPES.TRANSITION_CURRENT]: {
                shape: 'box',
                borderWidth: 3,
                shapeProperties: { borderRadius: 0 },
                color: { border: COLORS.YELLOW, background: COLORS.WHITE },
                mass: transitionMass

            },
            [Automata.NODE_TYPES.LOOP_CURRENT]: {
                shape: 'box',
                borderWidth: 3,
                shapeProperties: { borderRadius: 0 },
                color: { border: COLORS.YELLOW, background: COLORS.WHITE },
                mass: loopMass
            }
        }
    };

    return new Network(container, data, options);

}

let container = networkDiv.get(0);

const drawButton = $('<button>')
    .text('draw!')
    .on('click', () => {
        const automaton = window.AUTOMATON = new Automata(automatonDiv.val(), startInput.val(), finalInput.val());
        const data = window.DATA = automaton.getDataSets();
        window.NETWORK = drawAutomaton(container, data);
    })
    .appendTo(panelDiv);

$('<hr>').appendTo(panelDiv);

let inputTape = $('<div>')
    .appendTo(body)
    .addClass('input-tape');

let slideContainer = $('<div>')
    .appendTo(body)
    .addClass('slide-container');

$('<hr>').appendTo(panelDiv);

$('<label>').attr('for', 'final-input').text('Input').appendTo(panelDiv);

const displayComputation = (buttonEvent, inputInput) => {

    $(buttonEvent.target).addClass('disabled');

    inputTape.empty();
    const input = inputInput.val();
    let inputDivArray = [$('<div>').addClass('start symbol').text('>').appendTo(inputTape)];
    input.split('').forEach(symbol => {
        inputDivArray.push(
            $('<div>')
                .addClass('symbol')
                .text(symbol)
                .on('click', (e) => $(e.target).addClass('blinking'))
                .appendTo(inputTape)
        );
    });

    const computation = AUTOMATON.compute(input);
    const steps = [];

    inputDivArray.push(
        $('<div>')
            .addClass('end symbol')
            .addClass(computation.accept ? 'accepted' : 'rejected')
            .text(computation.accept ? '✔' : '✘')
            .appendTo(inputTape)
    );

    $.each(computation.path, (symbolIndex, step) => {
        $.each([
            step.transitions,
            step.states,
            ...((AUTOMATON.hasLambdaTransitions() &&
                step.lambdaTransitions.length !== step.states.length)?[
                step.lambdaTransitions,
                step.lambdaStates
            ]:[])
        ], (subStep, nodes) => {
            steps.push({
                network: {
                    selection: {nodes: nodes},
                },
                tape: {
                    current: symbolIndex,
                    subStep: subStep
                }
            })
        });
    });

    slideContainer.empty();

    const slider = $('<input type="range">')
        .attr('min', '0')
        .attr('max', '' + (inputDivArray.length-1))
        .val(0)
        .addClass('slider')
        .css('width', 'calc({em}em + {px}px'.format({em: inputDivArray.length, px: inputDivArray.length * 5}))
        .appendTo(slideContainer);

    const time = 500;

    $('.symbol').removeClass('consuming cursor-blinking consumed');
    inputTape.find('.start.symbol').addClass('consuming');

    $.each(steps, (idx, step) => {
        setTimeout(() => {

            slider.val(step.tape.current);

            DATA.nodes.update(
                DATA.nodes.get().map(node => ({
                    id: node.id,
                    group: node.group%2===0 ? node.group/2 : node.group
                }))
            );

            DATA.nodes.update(
                DATA.nodes.get().map(node => ({
                    id: node.id,
                    group: new Set(step.network.selection.nodes).has(node.id) ? node.group*2 : node.group
                }))
            );

            $.each(inputDivArray, (inputIdx, inputDiv) => {
                inputDiv.removeClass('consuming consumed cursor-blinking');
                inputDiv.addClass(inputIdx < step.tape.current ?
                    'consumed' :
                    inputIdx === step.tape.current ?
                        (step.tape.subStep === 0 ? 'consuming' : 'consumed cursor-blinking') :
                        '')
            });

        }, (idx)*time);
    });

    setTimeout(() => {
        $(buttonEvent.target).removeClass('disabled');
        $('.symbol').removeClass('consuming cursor-blinking').addClass('consumed');
        slider.val(steps.length+1);
    }, (steps.length)*time);

};

const inputInputs = $('<div>').addClass('input-inputs').appendTo(panelDiv);

const addExamples = (examples) => {
    inputInputs.empty();
    ['', ...examples].forEach((example) => {
        const inputInput = $('<input>')
            .addClass('input')
            .val(example)
            .attr('placeholder', 'ε')
            .attr('id', 'input-input')
            .appendTo(inputInputs);
        const animateButton = $('<button>')
            .text('compute!')
            .on('click', (e) => displayComputation(e, inputInput))
            .appendTo(inputInputs);
    });
};



$('<div>')
    .append($('<div>').text('Examples'))
    .append($('<button>')
        .text('w contains ab')
        .on('click', () => {
            automatonDiv.val(
                'q0 a q0\n' +
                'q0 b q0\n' +
                'q0 a q1\n' +
                'q1 b qf\n' +
                'qf a qf\n' +
                'qf b qf'
            );
            startInput.val('q0');
            finalInput.val('qf');
            drawButton.click();
            addExamples(['ab', 'bbbaaa', 'bbaabba']);
        })
    )
    .append($('<button>')
        .text('w ends in 01')
        .on('click', () => {
            automatonDiv.val(
                'q0 0 q0\n' +
                'q0 1 q0\n' +
                'q0 0 q1\n' +
                'q1 1 qf'
            );
            startInput.val('q0');
            finalInput.val('qf');
            drawButton.click();
            addExamples(['01', '000101', '001010010']);
        })
    )
    .append($('<button>')
        .text('w doesn\'t contain abc')
        .on('click', () => {
            automatonDiv.val(
                'q0 a q1\n' +
                'q0 b q0\n' +
                'q0 c q0\n' +
                'q1 a q1\n' +
                'q1 b q2\n' +
                'q1 c q0\n' +
                'q2 a q1\n' +
                'q2 b q0\n' +
                'q2 c qs\n' +
                'qs a qs\n' +
                'qs b qs\n' +
                'qs c qs'
            );
            startInput.val('q0');
            finalInput.val('q0, q1, q2');
            drawButton.click();
            addExamples(['aabbcc', 'abc', 'abbcbcbabcaab']);
        })
    )
    .append($('<button>')
        .text('w contains ab but not abc')
        .addClass('hidden')
        .on('click', () => {
            automatonDiv.val(
                'q0 a q1\n' +
                'q0 b q0\n' +
                'q0 c q0\n' +
                'q1 a q1\n' +
                'q1 b qf1\n' +
                'q1 c q0\n' +
                'qf1 c qs\n' +
                'qf1 b qf2\n' +
                'qf1 a qf3\n' +
                'qf2 b qf2\n' +
                'qf2 c qf2\n' +
                'qf2 a qf3\n' +
                'qf3 b qf4\n' +
                'qf3 c qf2\n' +
                'qf3 a qf3\n' +
                'qf4 a qf3\n' +
                'qf4 b qf2\n' +
                'qf4 c qs'
            );
            startInput.val('q0');
            finalInput.val('qf1, qf2, qf3, qf4');
            drawButton.click();
            addExamples(['aabbcc', 'baaccbaacbbbac', 'abbcbcbabcaab']);
        })
    )
    .append($('<button>')
        .text('w = 0ⁿ, n is a multiple of 2 or a multiple of 3')
        .on('click', () => {
            automatonDiv.val(
                'q0 _ q02\n' +
                'q02 0 q12\n' +
                'q12 0 q02\n' +
                'q02 _ qf\n' +
                'q0 _ q03\n' +
                'q03 0 q13\n' +
                'q13 0 q23\n' +
                'q23 0 q03\n' +
                'q03 _ qf'
            );
            startInput.val('q0');
            finalInput.val('qf');
            drawButton.click();
            addExamples(['0000000', '000000000', '00', '000000']);
        })
    )
    .appendTo(panelDiv);
